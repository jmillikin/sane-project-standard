# SANE Standard

Converted from the original LaTeX version, this is the SANE Standard
in reStructuredText format, for use with Sphinx.  You can find the
latest published version at

  https://sane-project.gitlab.io/standard/

where you can also find links to HTML and PDF exports for all tagged
versions and branches.

## Building A Single Version

You can build HTML and PDF versions of the SANE Standard with one of

``` sh
sphinx-build -M html . _build/
sphinx-build -M latexpdf . build/
```

This assumes an environment where not only the [requirements][RQ] are
installed but also [Sphinx][SX] itself and, for PDF output, a number
of [LaTeX][LX] related packages.  For an *example* setup, please refer
to the [Dockerfile][DF] for the image used by this project's [CI][CI].
While the example is Debian specific, it should be helpful in getting
started on another distribution, if so inclined.  Of course, using the
Docker image that is used in CI is another option.

## Building All Tagged Versions

The CI setup for this project publishes HTML and PDF versions for all
tagged versions.  You can create all these versions locally too.

In addition to everything that is needed to build the latest version,
you will need to install the `sphinx-versions` Python package.  If you
installed everything listed in [requirements.txt][RQ], you are all
set.

The build itself is done with

``` sh
CI=true sphinx-versioning build -P sane-standard.pdf . public/
```

Of course, all this can be found back in the [.gitlab-ci.yml][CI] file
as well.

## How Did This All Come About

The SANE Standard was first released as part of `sane-1.0.1`.  It was
maintained in [LaTeX][LX] format in two files, `doc/sane.tex` and
`doc/net.tex`, and a small number of images created with [xfig][XF]
and kept below `docs/figs/`.  When the `sane` package was split into
two packages, `sane-backends` and `sane-frontends`, the SANE Standard
ended up in the former.  It was infrequently edited over time and
occasionally the version was bumped (see the [Changelog][CL] for
details).

The SANE Project's website included an HTML and printable version of
the SANE Standard.  The printable version started out as a PostScript
document but in the summer of 2016 a PDF version was added.  There
were a few problems with how the SANE Standard was maintained on the
website however.

For one thing, all of the HTML, PostScript and PDF versions required
manual updates whenever the SANE Standard changed.  Second, the tool
used to convert to HTML was no longer functional/available.  Third,
the HTML version looked extremely twentieth century or retro, if you
wish, and was cumbersome to navigate.

In the fall of 2019, @paddy-hack set out to convert the SANE Standard
to [reStructuredText][RE] for use with [Sphinx][SX].  He reconstructed
the standard's commit history in a `git` repository of its own (in the
`latex-master` and `latex-draft-2` branches) and diligently worked his
way through all commits converting one at a time (on the `master` and
`draft-2` branches).  While taking utmost care to stay true to the
LaTeX revisions and neither introduce nor fix mistakes, discrepancies
in the reStructuredText version are to blame solely on @paddy-hack.
The project was finally made public mid February 2020, after being
interrupted by preparations for and release of `sane-backends-1.0.29`.

 [CI]: .gitlab-ci.yml
 [CL]: CHANGELOG.md
 [DF]: https://gitlab.com/paddy-hack/sphinx/blob/master/debian.df
 [LX]: https://www.latex-project.org/
 [RE]: https://en.wikipedia.org/wiki/ReStructuredText
 [RQ]: requirements.txt
 [SX]: https://www.sphinx-doc.org/
 [XF]: https://www.xfig.org/
