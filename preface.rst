Preface
=======

The SANE standard is being developed by a group of free-software
developers. The process is open to the public and comments as well as
suggestions for improvements are welcome. Information on how to join the
SANE development process can be found in Chapter
:numref:`chap:contact`.

The SANE standard is intended to streamline software development by
providing a standard application programming interface to access raster
scanner hardware. This should reduce the number of different driver
implementations, thereby reducing the need for reimplementing similar
code.

About This Document
-------------------

This document is intended for developers who are creating either an
application that requires access to raster scanner hardware and for
developers who are implementing a SANE driver. It does not cover
specific implementations of SANE components. Its sole purpose is to
describe and define the SANE application interface that will enable any
application on any platform to interoperate with any SANE backend for
that platform.

The remainder of this document is organized as follows.
Chapter :numref:`chap:intro` provides introductory material.
Chapter :numref:`chap:environ` presents the environment SANE is
designed for. Chapter :numref:`chap:api` details the SANE
Application Programmer Interface. Chapter :numref:`chap:net`
specifies the network protocol that can be used to implement the SANE
API in a network transparent fashion. Finally,
Chapter :numref:`chap:contact` gives information on how to join
the SANE development process.

Typographic Conventions
~~~~~~~~~~~~~~~~~~~~~~~

Changes since the last revision of this document are highlighted like
this:
