.. _chap:intro:

Introduction
============

SANE is an application programming interface (API) that provides
standardized access to any raster image scanner hardware. The
standardized interface allows to write just one driver for each scanner
device instead of one driver for each scanner and application. The
reduction in the number of required drivers provides significant savings
in development time. More importantly, SANE raises the level at which
applications can work. As such, it will enable applications that were
previously unheard of in the UNIX world. While SANE is primarily
targeted at a UNIX environment, the standard has been carefully designed
to make it possible to implement the API on virtually any hardware or
operating system.

SANE is an acronym for “Scanner Access Now Easy.” Also, the hope is that
SANE is sane in the sense that it will allow easy implementation of the
API while accommodating all features required by today’s scanner
hardware and applications. Specifically, SANE should be broad enough to
accommodate devices such as scanners, digital still and video cameras,
as well as virtual devices like image file filters.

Terminology
-----------

An application that uses the SANE interface is called a SANE *frontend*.
A driver that implements the SANE interface is called a SANE *backend*.
A *meta backend* provides some means to manage one or more other
backends.
