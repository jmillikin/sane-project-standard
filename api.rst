.. _chap:api:

The SANE Application Programmer Interface (API)
===============================================

This section defines version 1 of the SANE application
programmer interface (API). Any SANE frontend must depend on the
interface defined in this section only. Conversely, any SANE backend
must implement its functionality in accordance with this specification.
The interface as documented here is declared as a C callable interface
in a file called ``sane/sane.h``. This file should
normally be included via a C preprocessor directive of the form:

    ::

        #include <sane/sane.h>

.. _sec:saneversioncode:

Version Control
---------------

The SANE standard is expected to evolve over time. Whenever a change to
the SANE standard is made that may render an existing frontend or
backend incompatible with the new standard, the major version number
must be increased. Thus, any frontend/backend pair is compatible
provided the major version number of the SANE standard they implement is
the same. A frontend may implement backwards compatibility by allowing
major numbers that are smaller than the expected major number (provided
the frontend really can cope with the older version). In contrast, a
backend always provides support for one and only one version of the
standard. If a specific application does require that two different
versions of the same backend are accessible at the same time, it is
possible to do so by installing the two versions under different names.

SANE version control also includes a minor version number and a build
revision. While control of these numbers remains with the implementer of
a backend, the recommended use is as follows. The minor version is
incremented with each official release of a backend. The build revision
is increased with each build of a backend.

The SANE API provides the following five macros to manage version
numbers.

    .. macro:: SANE_CURRENT_MAJOR

        The value of this macro is the number of the SANE standard that
        the interface implements.

    .. function:: SANE_VERSION_CODE(maj, min, bld)

        This macro can be used
        to build a monotonically increasing version code. A SANE version
        code consists of the SANE standard major version number
        (:data:`maj`), the minor version number
        :data:`min`, and the build revision of a backend
        (:data:`bld`). The major and minor version numbers
        must be in the range 0…255 and the build revision must be in the
        range 0…65535.

        Version codes are monotonic in the sense that it is possible to
        apply relational operators (e.g., equality or less-than test)
        directly on the version code rather than individually on the
        three components of the version code.

        Note that the major version number alone determines whether a
        frontend/backend pair is compatible. The minor version and the
        build revision are used for informational and bug-fixing
        purposes only.

    .. function:: SANE_VERSION_MAJOR(vc)

        This macro returns the major version number component of the
        version code passed in argument :data:`vc`.

    .. function:: SANE_VERSION_MINOR(vc)

        This macro returns the minor version number component of the
        version code passed in argument :data:`vc`.

    .. function:: SANE_VERSION_BUILD(vc)

        This macro returns the build revision component of the version
        code passed in argument :data:`vc`.

Data Types
----------

.. index:: SANE_Byte, SANE_Word

Base Types
~~~~~~~~~~

The SANE standard is based on just two SANE-specific base types: the
SANE byte and word.

    ::

        typedef some-scalar-type SANE_Byte;
        typedef some-scalar-type SANE_Word;

:type:`SANE_Byte` must correspond to some scalar C type that is capable of
holding values in the range 0 to 255. :type:`SANE_Word` must be capable of
holding any of the following:

-  the truth values :macro:`SANE_FALSE` and :macro:`SANE_TRUE`

-  signed integers in the range :math:`-2^{31}\ldots2^{31}-1`

-  fixed point values in the range :math:`-32768\ldots32767.9999` with a
   resolution of :math:`1/65536`

-  32 bits (for bit sets)

Note that the SANE standard does not define what C type :type:`SANE_Byte`
and :type:`SANE_Word` map to. For example, on some platforms, the latter may
map to :type:`long int` whereas on others it may map to :type:`int`. A portable
SANE frontend or backend must therefore not depend on a particular
mapping.

.. index:: SANE_Bool, SANE_FALSE, SANE_TRUE

Boolean Type
~~~~~~~~~~~~

:type:`SANE_Bool` is used for variables that can take
one of the two truth values :macro:`SANE_FALSE` and
:macro:`SANE_TRUE`. The former value is defined to be
0, whereas the latter is 1. [1]_ The C declarations for this type are
given below.

    ::

        #define SANE_FALSE      0
        #define SANE_TRUE       1
        typedef SANE_Word SANE_Bool;

Note that :type:`SANE_Bool` is simply an alias of :type:`SANE_Word`. It is
therefore always legal to use the latter type in place of the former.
However, for clarity, it is recommended to use :type:`SANE_Bool` whenever a
given variable or formal argument has a fixed interpretation as a
Boolean object.

.. index:: SANE_Int

Integer Type
~~~~~~~~~~~~

:type:`SANE_Int` is used for variables that can take
integer values in the range :math:`-2^{32}` to :math:`2^{31}-1`. Its C
declaration is given below.

    ::

        typedef SANE_Word SANE_Int;

Note that :type:`SANE_Int` is simply an alias of :type:`SANE_Word`. It is
therefore always legal to use the latter type in place of the former.
However, for clarity, it is recommended to use :type:`SANE_Int` whenever a
given variable or formal argument has a fixed interpretation as an
integer object.

.. index:: SANE_Fixed, SANE_FIXED_SCALE_SHIFT

Fixed-point Type
~~~~~~~~~~~~~~~~

:type:`SANE_Fixed` is used for variables that can
take fixed point values in the range :math:`-32768` to
:math:`32767.9999` with a resolution of :math:`1/65535`. The C
declarations relating to this type are given below.

    ::

        #define SANE_FIXED_SCALE_SHIFT  16
        typedef SANE_Word SANE_Fixed;

The macro :macro:`SANE_FIXED_SCALE_SHIFT` gives the
location of the fixed binary point. This standard defines that value to
be 16, which yields a resolution of :math:`1/65536`.

Note that :type:`SANE_Fixed` is simply an alias of :type:`SANE_Word`. It is
therefore always legal to use the latter type in place of the former.
However, for clarity, it is recommended to use :type:`SANE_Fixed` whenever a
given variable or formal argument has a fixed interpretation as a
fixed-point object.

For convenience, SANE also defines two macros that convert fixed-point
values to and from C double floating point values.

    .. function:: SANE_FIX(d)

        Returns the largest SANE fixed-point value that is smaller than
        the double value :data:`d`. No range checking is
        performed. If the value of :data:`d` is out of range,
        the result is undefined.

    .. function:: SANE_UNFIX(w)

        Returns the nearest double machine number that corresponds to
        fixed-point value :data:`w`.

SANE does *not* require that the following two expressions hold true
(even if the values of :data:`w` and :data:`d` are
in range):

    ::

        SANE_UNFIX(SANE_FIX(d)) == d
        SANE_FIX(SANE_UNFIX(w)) == w

In other words, conversion between fixed and double values may be lossy.
It is therefore recommended to avoid repeated conversions between the
two representations.

Text
~~~~

.. index:: SANE_Char

Character Type
^^^^^^^^^^^^^^

Type :type:`SANE_Char` represents a single text
character or symbol. At present, this type maps directly to the
underlying C :type:`char` type (typically one byte). The encoding for such
characters is currently fixed as ISO LATIN-1. Future versions of this
standard may map this type to a wider type and allow multi-byte
encodings to support internationalization. As a result of this, care
should be taken to avoid the assumption that
:math:`\verb|sizeof|(\verb|SANE_Char|) == \verb|sizeof|(\verb|char|)`.

    ::

        typedef char SANE_Char;

.. index:: SANE_String, SANE_String_Const, NUL

String Type
^^^^^^^^^^^

Type :type:`SANE_String` represents a text string as
a sequence of C :type:`char` values. The end of the sequence is indicated by
a ``'\0'`` (:data:`NUL`) character.

    ::

        typedef SANE_Char *SANE_String;
        typedef const SANE_Char *SANE_String_Const;

The type :type:`SANE_String_Const` is provided by
SANE to enable declaring strings whose contents is unchangeable. Note
that in ANSI C, the declaration

    ::

        const SANE_String str;

declares a string pointer that is constant (not a string pointer that
points to a constant value).

.. index:: SANE_Handle

Scanner Handle Type
~~~~~~~~~~~~~~~~~~~

Access to a scanner is provided through an opaque type called
:type:`SANE_Handle`. The C declaration of this type
is given below.

    ::

        typedef void *SANE_Handle;

While this type is declared to be a void pointer, an application must
not attempt to interpret the value of a :type:`SANE_Handle`. In particular,
SANE does not require that a value of this type is a legal pointer
value.

.. index:: SANE_Status

Status Type
~~~~~~~~~~~

Most SANE operations return a value of type
:type:`SANE_Status` to indicate whether the
completion status of the operation. If an operation completes
successfully, :macro:`SANE_STATUS_GOOD` is returned. In case of an error, a
value is returned that indicates the nature of the problem. The complete
list of available status codes is listed in
:numref:`tab:status`. It is recommended to use function
:func:`sane_strstatus()` to convert status codes into a
legible string.

.. tabularcolumns::  |\X{12}{32}|r|l|
.. table:: Status Codes
   :name: tab:status
   :align: center

   +--------------------------------------+------+---------------------------------------+
   | Symbol                               | Code | Description                           |
   +======================================+======+=======================================+
   | .. macro:: SANE_STATUS_GOOD          |    0 | Operation completed successfully.     |
   +--------------------------------------+------+---------------------------------------+
   | .. macro:: SANE_STATUS_UNSUPPORTED   |    1 | Operation is not supported.           |
   +--------------------------------------+------+---------------------------------------+
   | .. macro:: SANE_STATUS_CANCELLED     |    2 | Operation was cancelled.              |
   +--------------------------------------+------+---------------------------------------+
   | .. macro:: SANE_STATUS_DEVICE_BUSY   |    3 | Device is busy—retry later.           |
   +--------------------------------------+------+---------------------------------------+
   | .. macro:: SANE_STATUS_INVAL         |    4 | Data or argument is invalid.          |
   +--------------------------------------+------+---------------------------------------+
   | .. macro:: SANE_STATUS_EOF           |    5 | No more data available (end-of-file). |
   +--------------------------------------+------+---------------------------------------+
   | .. macro:: SANE_STATUS_JAMMED        |    6 | Document feeder jammed.               |
   +--------------------------------------+------+---------------------------------------+
   | .. macro:: SANE_STATUS_NO_DOCS       |    7 | Document feeder out of documents.     |
   +--------------------------------------+------+---------------------------------------+
   | .. macro:: SANE_STATUS_COVER_OPEN    |    8 | Scanner cover is open.                |
   +--------------------------------------+------+---------------------------------------+
   | .. macro:: SANE_STATUS_IO_ERROR      |    9 | Error during device I/O.              |
   +--------------------------------------+------+---------------------------------------+
   | .. macro:: SANE_STATUS_NO_MEM        |   10 | Out of memory.                        |
   +--------------------------------------+------+---------------------------------------+
   | .. macro:: SANE_STATUS_ACCESS_DENIED |   11 | Access to resource has been denied.   |
   +--------------------------------------+------+---------------------------------------+

.. index:: SANE_Device

Device Descriptor Type
~~~~~~~~~~~~~~~~~~~~~~

Each SANE device is represented by a structure of type
:type:`SANE_Device`. The C declaration of this type
is given below.

    ::

        typedef struct
          {
            SANE_String_Const name;
            SANE_String_Const vendor;
            SANE_String_Const model;
            SANE_String_Const type;
          }
        SANE_Device;

.. index:: device-name

The structure provides the unique name
of the scanner in member :member:`name`. It is this unique
name that should be passed in a call to :func:`sane_open()`.
The format of this name is completely up to the backend. The only
constraints are that the name is unique among all devices supported by
the backend and that the name is a legal SANE text string. To simplify
presentation of unique names, their length should not be excessive. It
is *recommended* that backends keep unique names below 32 characters in
length. However, applications *must* be able to cope with arbitrary
length unique names.

The remaining members in the device structure provide additional
information on the device corresponding to the unique name.
Specifically, members :member:`vendor`,
:member:`model`, and :member:`type` are single-line
strings that give information on the vendor (manufacturer), model, and
the type of the device. For consistency’s sake, the following strings
should be used when appropriate (the lists will be expanded as need
arises):

.. tabularcolumns:: |L|L|
.. table:: Predefined Device Information Strings
   :align: center
   :name: vendor-names

   +---------------------+----------------+
   |  Vendor Strings                      |
   +=====================+================+
   | ``AGFA``            | ``Microtek``   |
   +---------------------+----------------+
   | ``Abaton``          | ``Minolta``    |
   +---------------------+----------------+
   | ``Acer``            | ``Mitsubishi`` |
   +---------------------+----------------+
   | ``Apple``           | ``Mustek``     |
   +---------------------+----------------+
   | ``Artec``           | ``NEC``        |
   +---------------------+----------------+
   | ``Avision``         | ``Nikon``      |
   +---------------------+----------------+
   | ``CANON``           | ``Plustek``    |
   +---------------------+----------------+
   | ``Connectix``       | ``Polaroid``   |
   +---------------------+----------------+
   | ``Epson``           | ``Relisys``    |
   +---------------------+----------------+
   | ``Fujitsu``         | ``Ricoh``      |
   +---------------------+----------------+
   | ``Hewlett-Packard`` | ``Sharp``      |
   +---------------------+----------------+
   | ``IBM``             | ``Siemens``    |
   +---------------------+----------------+
   | ``Kodak``           | ``Tamarack``   |
   +---------------------+----------------+
   | ``Lexmark``         | ``UMAX``       |
   +---------------------+----------------+
   | ``Logitech``        | ``Noname``     |
   +---------------------+----------------+

.. tabularcolumns:: |L|
.. table::
   :align: center

   +-------------------------------+
   | Type Strings                  |
   +===============================+
   | ``film scanner``              |
   +-------------------------------+
   | ``flatbed scanner``           |
   +-------------------------------+
   | ``frame grabber``             |
   +-------------------------------+
   | ``handheld scanner``          |
   +-------------------------------+
   | ``multi-function peripheral`` |
   +-------------------------------+
   | ``sheetfed scanner``          |
   +-------------------------------+
   | ``still camera``              |
   +-------------------------------+
   | ``video camera``              |
   +-------------------------------+
   | ``virtual device``            |
   +-------------------------------+

Note that vendor string ``Noname`` can be used for
virtual devices that have no physical vendor associated. Also, there are
no predefined model name strings since those are vendor specific and
therefore completely under control of the respective backends.

.. index:: SANE_Option_Descriptor
.. _sec:odesc:

Option Descriptor Type
~~~~~~~~~~~~~~~~~~~~~~

Option descriptors are at the same time the most intricate and powerful
type in the SANE standard. Options are used to control virtually all
aspects of device operation. Much of the power of the SANE API stems
from the fact that most device controls are completely described by
their respective option descriptor. Thus, a frontend can control a
scanner abstractly, without requiring knowledge as to what the purpose
of any given option is. Conversely, a scanner can describe its controls
without requiring knowledge of how the frontend operates. The C
declaration of the :type:`SANE_Option_Descriptor`
type is given below.

    ::

        typedef struct
          {
            SANE_String_Const name;
            SANE_String_Const title;
            SANE_String_Const desc;
            SANE_Value_Type type;
            SANE_Unit unit;
            SANE_Int size;
            SANE_Int cap;
            SANE_Constraint_Type constraint_type;
            union
              {
                const SANE_String_Const *string_list;
                const SANE_Word *word_list;
                const SANE_Range *range;
              }
            constraint;
          }
        SANE_Option_Descriptor;

Option Name
^^^^^^^^^^^

Member :member:`name` is a string that uniquely identifies the
option. The name must be unique for a given device (i.e., the option
names across different backends or devices need not be unique). The
option name must consist of lower-case ASCII letters
(``a``–``z``), digits
(``0``–``9``), or the dash character
(``-``) only. The first character must be a lower-case
ASCII character (i.e., not a digit or a dash).

Option Title
^^^^^^^^^^^^

Member :member:`title` is a single-line string that can be
used by the frontend as a title string. This should typically be a short
(one or two-word) string that is chosen based on the function of the
option.

Option Description
^^^^^^^^^^^^^^^^^^

Member :member:`desc` is a (potentially very) long string that
can be used as a help text to describe the option. It is the
responsibility of the frontend to break the string into manageable-length
lines. Newline characters in this string should be interpreted as
paragraph breaks.

.. index:: SANE_Value_Type

Option Value Type
^^^^^^^^^^^^^^^^^

Member :member:`type` specifies the type of the option value.
The possible values for type :type:`SANE_Value_Type`
are described in :numref:`tab:valuetype`.


.. tabularcolumns:: |\X{8}{32}|r|\X{20}{32}|
.. table:: Option Value Types (:type:`SANE_Value_Type`)
   :name: tab:valuetype
   :align: center

   +-----------------------------+------+------------------------------------------------------------------------+
   | Symbol                      | Code | Description                                                            |
   +=============================+======+========================================================================+
   | .. macro:: SANE_TYPE_BOOL   | 0    | Option value is of type :type:`SANE_Bool`.                             |
   +-----------------------------+------+------------------------------------------------------------------------+
   | .. macro:: SANE_TYPE_INT    | 1    | Option value is of type :type:`SANE_Int`.                              |
   +-----------------------------+------+------------------------------------------------------------------------+
   | .. macro:: SANE_TYPE_FIXED  | 2    | Option value is of type :type:`SANE_Fixed`.                            |
   +-----------------------------+------+------------------------------------------------------------------------+
   | .. macro:: SANE_TYPE_STRING | 3    | Option value is of type :type:`SANE_String`.                           |
   +-----------------------------+------+------------------------------------------------------------------------+
   | .. macro:: SANE_TYPE_BUTTON | 4    | An option of this type has no value.                                   |
   |                             |      | Instead, setting an option of this type has an option-specific         |
   |                             |      | side-effect.  For example, a button-typed option could be used by a    |
   |                             |      | backend to provide a means to select default values or to the tell an  |
   |                             |      | automatic document feeder to advance to the next sheet of paper.       |
   +-----------------------------+------+------------------------------------------------------------------------+
   | .. macro:: SANE_TYPE_GROUP  | 5    | An option of this type has no value.                                   |
   |                             |      | This type is used to group logically related options.  A group option  |
   |                             |      | is in effect up to the point where another group option is encountered |
   |                             |      | (or up to the end of the option list, if there are no other group      |
   |                             |      | options).  For group options, only members :member:`title` and         |
   |                             |      | :member:`type` are valid in the option descriptor.                     |
   +-----------------------------+------+------------------------------------------------------------------------+

.. index:: SANE_Unit

Option Value Unit
^^^^^^^^^^^^^^^^^

Member :member:`unit` specifies what the physical unit of the
option value is. The possible values for type
:type:`SANE_Unit` are described in
:numref:`tab:units`. Note that the specified unit is what the
SANE backend expects. It is entirely up to a frontend as to how these
units a presented to the user. For example, SANE expresses all lengths
in millimeters. A frontend is generally expected to provide appropriate
conversion routines so that a user can express quantities in a customary
unit (e.g., inches or centimeters).

.. tabularcolumns:: |\X{10}{32}|r|l|
.. table:: Physical Units (:type:`SANE_Unit`)
   :name: tab:units
   :align: center

   +----------------------------------+------+----------------------------------------+
   | Symbol                           | Code | Description                            |
   +==================================+======+========================================+
   | .. macro:: SANE_UNIT_NONE        | 0    | Value is unit-less (e.g., page count). |
   +----------------------------------+------+----------------------------------------+
   | .. macro:: SANE_UNIT_PIXEL       | 1    | Value is in number of pixels.          |
   +----------------------------------+------+----------------------------------------+
   | .. macro:: SANE_UNIT_BIT         | 2    | Value is in number of bits.            |
   +----------------------------------+------+----------------------------------------+
   | .. macro:: SANE_UNIT_MM          | 3    | Value is in millimeters.               |
   +----------------------------------+------+----------------------------------------+
   | .. macro:: SANE_UNIT_DPI         | 4    | Value is a resolution in dots/inch.    |
   +----------------------------------+------+----------------------------------------+
   | .. macro:: SANE_UNIT_PERCENT     | 5    | Value is a percentage.                 |
   +----------------------------------+------+----------------------------------------+
   | .. macro:: SANE_UNIT_MICROSECOND | 6    | Value is time in :math:`\mu`-seconds.  |
   +----------------------------------+------+----------------------------------------+

.. _sec:valuesize:

Option Value Size
^^^^^^^^^^^^^^^^^

Member :member:`size` specifies the size of the option value
(in bytes). This member has a slightly different interpretation
depending on the type of the option value:

    :macro:`SANE_TYPE_STRING`:
    The size is the maximum size of the string. For the purpose of
    string size calculations, the terminating :data:`NUL`
    character is considered to be part of the string. Note that the
    terminating :data:`NUL` character must always be
    present in string option values.

    :macro:`SANE_TYPE_INT`, :macro:`SANE_TYPE_FIXED`:
    The size must be a positive integer multiple of the size of a
    :type:`SANE_Word`. The option value is a vector of length
    :math:`\verb|size| / \verb|sizeof|(\verb|SANE_Word|)`.

    :macro:`SANE_TYPE_BOOL`:
    The size must be set to :math:`\verb|sizeof|(\verb|SANE_Word|)`.

    :macro:`SANE_TYPE_BUTTON`, :macro:`SANE_TYPE_GROUP`:
    The option size is ignored.

Option Capabilities
^^^^^^^^^^^^^^^^^^^

Member :member:`cap` describes what capabilities the option
possesses. This is a bitset that is formed as the inclusive logical OR of
the capabilities described in :numref:`tab:capabilities`.
The SANE API provides the following to macros to test certain features
of a given capability bitset:

    .. function:: SANE_OPTION_IS_ACTIVE(cap)

        This macro returns :macro:`SANE_TRUE` if and only if
        the option with the capability set :data:`cap` is
        currently active.

    .. function:: SANE_OPTION_IS_SETTABLE(cap)

        This macro returns :macro:`SANE_TRUE` if and only if
        the option with the capability set :data:`cap` is
        software settable.

.. tabularcolumns:: |\X{12}{32}|r|\X{16}{32}|
.. table:: Option Capabilities
   :name: tab:capabilities
   :align: center

   +---------------------------------+------+---------------------------------------------------------------------------+
   | Symbol                          | Code | Description                                                               |
   +=================================+======+===========================================================================+
   | .. macro:: SANE_CAP_SOFT_SELECT |    1 | The option                                                                |
   |                                 |      | value can be set by a call to :func:`sane_control_option()`.              |
   +---------------------------------+------+---------------------------------------------------------------------------+
   | .. macro:: SANE_CAP_HARD_SELECT |    2 | The option value can be set by                                            |
   |                                 |      | user-intervention (e.g., by flipping a switch).  The user-interface       |
   |                                 |      | should prompt the user to execute the appropriate action to set such      |
   |                                 |      | an option.  This capability is mutually exclusive with                    |
   |                                 |      | SANE_CAP_SOFT_SELECT (either one of them can be set, but not both         |
   |                                 |      | simultaneously).                                                          |
   +---------------------------------+------+---------------------------------------------------------------------------+
   | .. macro:: SANE_CAP_SOFT_DETECT |    4 | The option                                                                |
   |                                 |      | value can be detected by software.  If                                    |
   |                                 |      | :macro:`SANE_CAP_SOFT_SELECT` is set, this capability *must*              |
   |                                 |      | be set.  If :macro:`SANE_CAP_HARD_SELECT` is set, this capability         |
   |                                 |      | may or may not be set.  If this capability is set but neither             |
   |                                 |      | :macro:`SANE_CAP_SOFT_SELECT` nor :macro:`SANE_CAP_HARD_SELECT`           |
   |                                 |      | are, then there is no way to control the option.  That is, the            |
   |                                 |      | option provides read-out of the current value only.                       |
   +---------------------------------+------+---------------------------------------------------------------------------+
   | .. macro:: SANE_CAP_EMULATED    |    8 | If set, this capability indicates                                         |
   |                                 |      | that an option is not directly supported by the device and is             |
   |                                 |      | instead emulated in the backend.  A sophisticated frontend may            |
   |                                 |      | elect to use its own (presumably better) emulation in lieu of an emulated |
   |                                 |      | option.                                                                   |
   +---------------------------------+------+---------------------------------------------------------------------------+
   | .. macro:: SANE_CAP_AUTOMATIC   |   16 | If set, this capability indicates                                         |
   |                                 |      | that the backend (or the device) is capable to picking a reasonable       |
   |                                 |      | option value automatically.  For such options, it is possible to          |
   |                                 |      | select automatic operation by calling :func:`sane_control_option()`       |
   |                                 |      | with an action value of :macro:`SANE_ACTION_SET_AUTO`.                    |
   +---------------------------------+------+---------------------------------------------------------------------------+
   | .. macro:: SANE_CAP_INACTIVE    |   32 | If set, this capability indicates                                         |
   |                                 |      | that the option is not currently active (e.g., because it's               |
   |                                 |      | meaningful only if another option is set to some other value).            |
   +---------------------------------+------+---------------------------------------------------------------------------+
   | .. macro:: SANE_CAP_ADVANCED    |   64 |                                                                           |
   |                                 |      | If set, this capability indicates that the option should be               |
   |                                 |      | considered an "advanced user option."  A frontend typically               |
   |                                 |      | displays such options in a less conspicuous way than regular options      |
   |                                 |      | (e.g., a command line interface may list such options last or a           |
   |                                 |      | graphical interface may make them available in a separate "advanced       |
   |                                 |      | settings" dialog).                                                        |
   +---------------------------------+------+---------------------------------------------------------------------------+

.. index:: SANE_Constraint_Type

Option Value Constraints
^^^^^^^^^^^^^^^^^^^^^^^^

It is often useful to constrain the values that an option can take. For
example, constraints can be used by a frontend to determine how to
represent a given option. Member :member:`constraint_type`
indicates what constraint is in effect for the option. The constrained
values that are allowed for the option are described by one of the union
members of member :member:`constraint`. The possible values of
type :type:`SANE_Constraint_Type` and the
interpretation of the :member:`constraint` union is described
in :numref:`tab:constraints`.

.. tabularcolumns:: |\X{12}{32}|r|\X{16}{32}|
.. table:: Option Value Constraints
   :name: tab:constraints
   :align: center

   +----------------------------------------+------+--------------------------------------------------------------------------------+
   | Symbol                                 | Code | Description                                                                    |
   +========================================+======+================================================================================+
   | .. macro:: SANE_CONSTRAINT_NONE        |    0 | The value is unconstrained.                                                    |
   |                                        |      | The option can take any of the values possible for the option's                |
   |                                        |      | type.                                                                          |
   +----------------------------------------+------+--------------------------------------------------------------------------------+
   | .. macro:: SANE_CONSTRAINT_RANGE       |    1 | This constraint is                                                             |
   |                                        |      | applicable to integer and fixed-point valued options only.  It                 |
   |                                        |      | constrains the option value to a possibly quantized range of                   |
   |                                        |      | numbers.  Option descriptor member :member:`constraint.range` points to        |
   |                                        |      | a range of the type :type:`SANE_Range`.  This type is illustrated              |
   |                                        |      | below:                                                                         |
   |                                        |      |                                                                                |
   |                                        |      | ::                                                                             |
   |                                        |      |                                                                                |
   |                                        |      |     typedef struct                                                             |
   |                                        |      |       {                                                                        |
   |                                        |      |         SANE_Word min;                                                         |
   |                                        |      |         SANE_Word max;                                                         |
   |                                        |      |         SANE_Word quant;                                                       |
   |                                        |      |       }                                                                        |
   |                                        |      |     SANE_Range;                                                                |
   |                                        |      |                                                                                |
   |                                        |      | All three members in this structure are interpreted according to the           |
   |                                        |      | option value type (:macro:`SANE_TYPE_INT` or :macro:`SANE_TYPE_FIXED`).        |
   |                                        |      | Members :member:`min` and :member:`max` specify the minimum and maximum        |
   |                                        |      | values, respectively.  If member :member:`quant` is non-zero, it               |
   |                                        |      | specifies the quantization value.  If :math:`l` is the minimum value, :math:`u`|
   |                                        |      | the maximum value and :math:`q` the (non-zero) quantization of a range,        |
   |                                        |      | then the legal values are :math:`v=k\cdot q+l` for all non-negative            |
   |                                        |      | integer values of :math:`k` such that :math:`v<=u`.                            |
   +----------------------------------------+------+--------------------------------------------------------------------------------+
   | .. macro:: SANE_CONSTRAINT_WORD_LIST   |    2 | This constraint is applicable                                                  |
   |                                        |      | to integer and fixed-point valued options only.  It constrains the             |
   |                                        |      | option value to a list of numeric values.  Option descriptor member            |
   |                                        |      | :member:`constraint.word_list` points to a list of words that                  |
   |                                        |      | enumerates the legal values.  The first element in that list is an             |
   |                                        |      | integer (:type:`SANE_Int`) that specifies the length of the list (not          |
   |                                        |      | counting the length itself).  The remaining elements in the list are           |
   |                                        |      | interpreted according to the type of the option value                          |
   |                                        |      | (:macro:`SANE_TYPE_INT` or :macro:`SANE_TYPE_FIXED`).                          |
   +----------------------------------------+------+--------------------------------------------------------------------------------+
   | .. macro:: SANE_CONSTRAINT_STRING_LIST |    3 | This constraint is                                                             |
   |                                        |      | applicable to string-valued options only.  It constrains the option            |
   |                                        |      | value to a list of strings.  The option descriptor member                      |
   |                                        |      | :member:`constraint.string_list` points to a :macro:`NULL` terminated          |
   |                                        |      | list of strings that enumerate the legal values for the option                 |
   |                                        |      | value.                                                                         |
   +----------------------------------------+------+--------------------------------------------------------------------------------+

Operations
----------

.. index:: sane_init

:func:`sane_init`
~~~~~~~~~~~~~~~~~

This function must be called before any other SANE function can be
called.  The behavior of a SANE backend is undefined if this function is
not called first or if the status code returned by :func:`sane_init()`
is different from :macro:`SANE_STATUS_GOOD`.  The version code of the
backend is returned in the value pointed to by :data:`version_code`.
If that pointer is :macro:`NULL`, no version code is returned.
Argument :data:`authorize` is either a pointer to a function that is
invoked when the backend requires authentication for a specific
resource or :macro:`NULL` if the frontend does not support
authentication.

    ::

        SANE_Status sane_init (SANE_Int * version_code,
                               SANE_Authorization_Callback authorize);

The authorization function may be called by a backend in response to any
of the following calls:

    - :func:`sane_open()`,
    - :func:`sane_control_option()`,
    - :func:`sane_start()`

If a backend was initialized without authorization function, then
authorization requests that cannot be handled by the backend itself will
fail automatically and the user may be prevented from accessing
protected resources. Backends are encouraged to implement means of
authentication that do not require user assistance. E.g., on a
multi-user system that authenticates users through a login process a
backend could automatically lookup the appropriate password based on
resource- and user-name.

.. index:: SANE_Authorization_Callback, domain, username, password

The authentication function type has the following declaration:

    ::

        #define SANE_MAX_USERNAME_LEN   128
        #define SANE_MAX_PASSWORD_LEN   128

        typedef void (*SANE_Authorization_Callback)
            (SANE_String_Const resource,
             SANE_Char username[SANE_MAX_USERNAME_LEN],
             SANE_Char password[SANE_MAX_PASSWORD_LEN]);

Three arguments are passed to the authorization function:
:data:`resource` is a string specifying the name of the
resource that requires authorization. A frontend should use this string
to build a user-prompt requesting a username and a password. The
:data:`username` and :data:`password` arguments
are (pointers to) an array of :macro:`SANE_MAX_USERNAME_LEN`
and :macro:`SANE_MAX_PASSWORD_LEN` characters, respectively.
The authorization call should place the entered username and password in
these arrays. The returned strings *must* be ASCII-NUL terminated.

.. index:: sane_exit

:func:`sane_exit`
~~~~~~~~~~~~~~~~~

This function must be called to terminate use of a backend. The function
will first close all device handles that still might be open (it is
recommended to close device handles explicitly through a call to
:func:`sane_close()`, but backends are required to release
all resources upon a call to this function). After this function
returns, no function other than :func:`sane_init()` may be
called (regardless of the status value returned by
:func:`sane_exit()`. Neglecting to call this function may
result in some resources not being released properly.

    ::

        void sane_exit (void);

.. index:: sane_get_devices

:func:`sane_get_devices`
~~~~~~~~~~~~~~~~~~~~~~~~

This function can be used to query the list of devices that are
available. If the function executes successfully, it stores a pointer to
a :macro:`NULL` terminated array of pointers to
:type:`SANE_Device` structures in :data:`*device_list`. The
returned list is guaranteed to remain unchanged and valid until (a)
another call to this function is performed or (b) a call to
:func:`sane_exit()` is performed. This function can be
called repeatedly to detect when new devices become available. If
argument :data:`local_only` is true, only local devices are
returned (devices directly attached to the machine that SANE is running
on). If it is false, the device list includes all remote devices that
are accessible to the SANE library.

    ::

        SANE_Status sane_get_devices (const SANE_Device *** device_list,
                                      SANE_Bool local_only);

This function may fail with :macro:`SANE_STATUS_NO_MEM` if an
insufficient amount of memory is available.

    **Backend Implementation Note**

    SANE does not require that this function is called before a
    :func:`sane_open()` call is performed. A device name may
    be specified explicitly by a user which would make it unnecessary
    and undesirable to call this function first.

.. index:: sane_open

:func:`sane_open`
~~~~~~~~~~~~~~~~~

This function is used to establish a connection to a particular device.
The name of the device to be opened is passed in argument
:data:`name`. If the call completes successfully, a handle
for the device is returned in :data:`*h`. As a special case,
specifying a zero-length string as the device requests opening the first
available device (if there is such a device).

    ::

        SANE_Status sane_open (SANE_String_Const name, SANE_Handle * h);

This function may fail with one of the following status codes.

    :macro:`SANE_STATUS_DEVICE_BUSY`:
    The device is currently busy (in use by somebody else).

    :macro:`SANE_STATUS_INVAL`:
    The device name is not valid.

    :macro:`SANE_STATUS_IO_ERROR`:
    An error occurred while communicating with the device.

    :macro:`SANE_STATUS_NO_MEM`:
    An insufficient amount of memory is available.

    :macro:`SANE_STATUS_ACCESS_DENIED`:
    Access to the device has been denied due to insufficient or
    invalid authentication.

.. index:: sane_close

:func:`sane_close`
~~~~~~~~~~~~~~~~~~

This function terminates the association between the device handle
passed in argument :data:`h` and the device it represents.
If the device is presently active, a call to
:func:`sane_cancel()` is performed first. After this
function returns, handle :data:`h` must not be used anymore.

    ::

        void sane_close (SANE_Handle h);

.. index:: sane_get_option_descriptor

:func:`sane_get_option_descriptor`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This function is used to access option descriptors. The function returns
the option descriptor for option number :data:`n` of the
device represented by handle :data:`h`. Option number 0 is
guaranteed to be a valid option. Its value is an integer that specifies
the number of options that are available for device handle
:data:`h` (the count includes option 0). If :math:`n` is not
a valid option index, the function returns :macro:`NULL`. The
returned option descriptor is guaranteed to remain valid (and at the
returned address) until the device is closed.

    ::

        const SANE_Option_Descriptor *
            sane_get_option_descriptor (SANE_Handle h, SANE_Int n);

.. index:: sane_control_option
.. _sec:control:

:func:`sane_control_option`
~~~~~~~~~~~~~~~~~~~~~~~~~~~

This function is used to set or inquire the current value of option
number :data:`n` of the device represented by handle
:data:`h`. The manner in which the option is controlled is
specified by parameter :data:`a`. The possible values of
this parameter are described in more detail below. The value of the
option is passed through argument :data:`v`. It is a pointer
to the memory that holds the option value. The memory area pointed to by
:data:`v` must be big enough to hold the entire option value
(determined by member :member:`size` in the corresponding
option descriptor). The only exception to this rule is that when setting
the value of a string option, the string pointed to by argument
:data:`v` may be shorter since the backend will stop reading
the option value upon encountering the first :data:`NUL`
terminator in the string. If argument :data:`i` is not
:macro:`NULL`, the value of :data:`*i` will be
set to provide details on how well the request has been met. The meaning
of this argument is described in more detail below.

    ::

        SANE_Status sane_control_option (SANE_Handle h, SANE_Int n,
                                         SANE_Action a, void *v,
                                         SANE_Int * i);

.. index:: SANE_Action

The way the option is affected by a call to this function is controlled
by parameter :data:`a` which is a value of type
:type:`SANE_Action`. The possible values and their
meaning is described in :numref:`tab:actions`.

.. tabularcolumns:: |\X{10}{32}|r|\X{18}{32}|
.. table:: Action Values (:type:`SANE_Action`)
   :name: tab:actions
   :align: center

   +----------------------------------+------+-------------------------------------------------------------------------+
   | Symbol                           | Code | Description                                                             |
   +==================================+======+=========================================================================+
   | .. macro:: SANE_ACTION_GET_VALUE |    0 | Get current option value.                                               |
   +----------------------------------+------+-------------------------------------------------------------------------+
   | .. macro:: SANE_ACTION_SET_VALUE |    1 | Set option value.  The                                                  |
   |                                  |      | option value passed through argument :data:`v` may be modified by the   |
   |                                  |      | backend if the value cannot be set exactly.                             |
   +----------------------------------+------+-------------------------------------------------------------------------+
   | .. macro:: SANE_ACTION_SET_AUTO  |    2 | Turn on automatic mode.  Backend                                        |
   |                                  |      | or device will automatically select an appropriate value.  This mode    |
   |                                  |      | remains effective until overridden by an explicit set value request.    |
   |                                  |      | The value of parameter :data:`v` is completely ignored in this case and |
   |                                  |      | may be :macro:`NULL`.                                                   |
   +----------------------------------+------+-------------------------------------------------------------------------+

After setting a value via an action value of :macro:`SANE_ACTION_SET_VALUE`,
additional information on how well the request has been met is returned
in :data:`*i` (if :data:`i` is
non-:macro:`NULL`). The returned value is a bitset that may
contain any combination of the values described in
:numref:`tab:info`.

.. tabularcolumns:: |\X{11}{32}|r|\X{17}{32}|
.. table:: Additional Information Returned When Setting an Option
   :name: tab:info
   :align: center

   +-------------------------------------+------+----------------------------------------------------------------------+
   | Symbol                              | Code | Description                                                          |
   +=====================================+======+======================================================================+
   | .. macro:: SANE_INFO_INEXACT        |    1 | This value is returned when                                          |
   |                                     |      | setting an option value resulted in a value being selected that does |
   |                                     |      | not exactly match the requested value.  For example, if a scanner    |
   |                                     |      | can adjust the resolution in increments of 30dpi only, setting the   |
   |                                     |      | resolution to 307dpi may result in an actual setting of 300dpi.      |
   |                                     |      | When this happens, the bitset returned in :data:`*i` has this member |
   |                                     |      | set.  In addition, the option value is modified to reflect the       |
   |                                     |      | actual (rounded) value that was used by the backend.  Note that      |
   |                                     |      | inexact values are admissible for strings as well.  A backend may    |
   |                                     |      | choose to "round" a string to the closest matching legal string      |
   |                                     |      | for a constrained string value.                                      |
   +-------------------------------------+------+----------------------------------------------------------------------+
   | .. macro:: SANE_INFO_RELOAD_OPTIONS |    2 | The setting of an                                                    |
   |                                     |      | option may affect the value or availability of one or more           |
   |                                     |      | *other* options.  When this happens, the SANE backend sets this      |
   |                                     |      | member in :data:`*i` to indicate that the application should reload  |
   |                                     |      | all options.  This member may be set if and only if at least one     |
   |                                     |      | option changed.                                                      |
   +-------------------------------------+------+----------------------------------------------------------------------+
   | .. macro:: SANE_INFO_RELOAD_PARAMS  |    4 | The setting of an option may                                         |
   |                                     |      | affect the parameter values (see :func:`sane_get_parameters()`).     |
   |                                     |      | If setting an option affects the parameter values, this member will  |
   |                                     |      | be set in :data:`*i`.  Note that this member may be set even if the  |
   |                                     |      | parameters did not actually change.  However, it is guaranteed that  |
   |                                     |      | the parameters never change without this member being set.           |
   +-------------------------------------+------+----------------------------------------------------------------------+

This function may fail with one of the following status codes.

    :macro:`SANE_STATUS_UNSUPPORTED`:
    The operation is not supported for the specified handle and
    option number.

    :macro:`SANE_STATUS_INVAL`:
    The option value is not valid.

    :macro:`SANE_STATUS_IO_ERROR`:
    An error occurred while communicating with the device.

    :macro:`SANE_STATUS_NO_MEM`:
    An insufficient amount of memory is available.

    :macro:`SANE_STATUS_ACCESS_DENIED`:
    Access to the option has been denied due to insufficient or
    invalid authentication.

.. index:: sane_get_parameters

:func:`sane_get_parameters`
~~~~~~~~~~~~~~~~~~~~~~~~~~~

This function is used to obtain the current scan parameters. The
returned parameters are guaranteed to be accurate between the time a
scan has been started (:func:`sane_start()` has been called)
and the completion of that request. Outside of that window, the returned
values are best-effort estimates of what the parameters will be when
:func:`sane_start()` gets invoked. Calling this function
before a scan has actually started allows, for example, to get an
estimate of how big the scanned image will be. The parameters passed to
this function are the handle :data:`h` of the device for
which the parameters should be obtained and a pointer
:data:`p` to a parameter structure. The parameter structure
is described in more detail below.

    ::

        SANE_Status sane_get_parameters (SANE_Handle h,
                                         SANE_Parameters * p);

.. index:: SANE_Parameters

The scan parameters are returned in a structure of type
:type:`SANE_Parameters`. The C declaration of this
structure is given below.

    ::

        typedef struct
          {
            SANE_Frame format;
            SANE_Bool last_frame;
            SANE_Int bytes_per_line;
            SANE_Int pixels_per_line;
            SANE_Int lines;
            SANE_Int depth;
          }
        SANE_Parameters;

.. index:: SANE_Frame

Member :member:`format` specifies the format of the next frame
to be returned. The possible values for type
:type:`SANE_Frame` are described in
:numref:`tab:frameformat`. The meaning of these values is
described in more detail in Section :numref:`sec:imageformat`.

.. tabularcolumns:: |\X{8}{32}|r|l|
.. table:: Frame Format (:type:`SANE_Frame`)
   :name: tab:frameformat
   :align: center

   +-----------------------------+------+-----------------------------------------+
   | Symbol                      | Code | Description                             |
   +=============================+======+=========================================+
   | .. macro:: SANE_FRAME_GRAY  |    0 | Band covering human visual range.       |
   +-----------------------------+------+-----------------------------------------+
   | .. macro:: SANE_FRAME_RGB   |    1 | Pixel-interleaved red/green/blue bands. |
   +-----------------------------+------+-----------------------------------------+
   | .. macro:: SANE_FRAME_RED   |    2 | Red band of a red/green/blue image.     |
   +-----------------------------+------+-----------------------------------------+
   | .. macro:: SANE_FRAME_GREEN |    3 | Green band of a red/green/blue image.   |
   +-----------------------------+------+-----------------------------------------+
   | .. macro:: SANE_FRAME_BLUE  |    4 | Blue band of a red/green/blue image.    |
   +-----------------------------+------+-----------------------------------------+

Member :member:`last_frame` is set to
:macro:`SANE_TRUE` if and only if the frame that is currently
being acquired (or the frame that will be acquired next if there is no
current frame) is the last frame of a multi frame image (e.g., the
current frame is the blue component of a red, green, blue image).

Member :member:`lines` specifies how many scan lines the frame
is comprised of. If this value is -1, the number of lines is not known a
priori and the frontend should call :func:`sane_read()`
until it returns a status of :macro:`SANE_STATUS_EOF`.

Member :member:`bytes_per_line` specifies the number of bytes
that comprise one scan line.

Member :member:`depth` specifies the number of bits per
sample.

Member :member:`pixels_per_line` specifies the number of
pixels that comprise one scan line.

Assume :math:`B` is the number of channels in the frame, then the bit
depth :math:`d` (as given by member :member:`depth`) and the
number of pixels per line :math:`n` (as given by this member
:member:`pixels_per_line`) are related to :math:`c`, the
number of bytes per line (as given by member
:member:`bytes_per_line`) as follows:

.. math::

   c >= \left\{
     \begin{array}{ll}
       B\cdot \lfloor (n + 7) / 8\rfloor & \mbox{if $d=1$}\\
       B\cdot n \cdot d / 8 & \mbox{if $d>1$}
     \end{array}
     \right.

Note that the number of bytes per line can be larger than the minimum
value imposed by the right side of this equation. A frontend must be
able to properly cope with such “padded” image formats.

.. index:: sane_start

:func:`sane_start`
~~~~~~~~~~~~~~~~~~

This function initiates acquisition of an image from the device
represented by handle :data:`h`.

    ::

        SANE_Status sane_start (SANE_Handle h);

This function may fail with one of the following status codes.

    :macro:`SANE_STATUS_CANCELLED`:
    The operation was cancelled through a call to
    :func:`sane_cancel()`.

    :macro:`SANE_STATUS_DEVICE_BUSY`:
    The device is busy. The operation should be retried later.

    :macro:`SANE_STATUS_JAMMED`:
    The document feeder is jammed.

    :macro:`SANE_STATUS_NO_DOCS`:
    The document feeder is out of documents.

    :macro:`SANE_STATUS_COVER_OPEN`:
    The scanner cover is open.

    :macro:`SANE_STATUS_IO_ERROR`:
    An error occurred while communicating with the device.

    :macro:`SANE_STATUS_NO_MEM`:
    An insufficient amount of memory is available.

    :macro:`SANE_STATUS_INVAL`:
    The scan cannot be started with the current set of options. The
    frontend should reload the option descriptors, as if
    :macro:`SANE_INFO_RELOAD_OPTIONS` had been returned from a call to
    :func:`sane_control_option()`, since the device's capabilities may
    have changed.

.. index:: sane_read

:func:`sane_read`
~~~~~~~~~~~~~~~~~

This function is used to read image data from the device represented by
handle :data:`h`. Argument :data:`buf` is a
pointer to a memory area that is at least :data:`maxlen`
bytes long. The number of bytes returned is stored in
:data:`*len`. A backend must set this to zero when
a status other than
:macro:`SANE_STATUS_GOOD` is returned). When the call
succeeds, the number of bytes returned can be anywhere in the range from
0 to :data:`maxlen` bytes.

    ::

        SANE_Status sane_read (SANE_Handle h, SANE_Byte * buf,
                               SANE_Int maxlen, SANE_Int * len);

If this function is called when no data is available, one of two things
may happen, depending on the I/O mode that is in effect for handle
:data:`h`.

#. If the device is in blocking I/O mode (the default mode), the call
   blocks until at least one data byte is available (or until some error
   occurs).

#. If the device is in non-blocking I/O mode, the call returns
   immediately with status :macro:`SANE_STATUS_GOOD` and
   with :data:`*len` set to zero.

The I/O mode of handle :data:`h` can be set via a call to
:func:`sane_set_io_mode()`.

This function may fail with one of the following status codes.

    :macro:`SANE_STATUS_CANCELLED`:
    The operation was cancelled through a call to
    :func:`sane_cancel()`.

    :macro:`SANE_STATUS_EOF`:
    No more data is available for the current frame.

    :macro:`SANE_STATUS_JAMMED`:
    The document feeder is jammed.

    :macro:`SANE_STATUS_NO_DOCS`:
    The document feeder is out of documents.

    :macro:`SANE_STATUS_COVER_OPEN`:
    The scanner cover is open.

    :macro:`SANE_STATUS_IO_ERROR`:
    An error occurred while communicating with the device.

    :macro:`SANE_STATUS_NO_MEM`:
    An insufficient amount of memory is available.

    :macro:`SANE_STATUS_ACCESS_DENIED`:
    Access to the device has been denied due to insufficient or
    invalid authentication.

.. index:: sane_cancel

:func:`sane_cancel`
~~~~~~~~~~~~~~~~~~~

This function is used to immediately or as quickly as possible cancel
the currently pending operation of the device represented by handle
:data:`h`.

    ::

        void sane_cancel (SANE_Handle h);

This function can be called at any time (as long as handle
:data:`h` is a valid handle) but usually affects
long-running operations only (such as image is acquisition). It is safe
to call this function asynchronously (e.g., from within a signal
handler). It is important to note that completion of this operation does
*not* imply that the currently pending operation has been cancelled. It
only guarantees that cancellation has been *initiated*. Cancellation
completes only when the cancelled call returns (typically with a status
value of :macro:`SANE_STATUS_CANCELLED`). Since the SANE API
does not require any other operations to be re-entrant, this implies
that a frontend must *not* call any other operation until the cancelled
operation has returned.

.. index:: sane_set_io_mode

:func:`sane_set_io_mode`
~~~~~~~~~~~~~~~~~~~~~~~~

This function is used to set the I/O mode of handle
:data:`h`. The I/O mode can be either blocking or
non-blocking. If argument :data:`m` is
:macro:`SANE_TRUE`, the mode is set to non-blocking mode,
otherwise it's set to blocking mode.
This function can be called only after a call to
:func:`sane_start()` has been performed.

    ::

        SANE_Status sane_set_io_mode (SANE_Handle h, SANE_Bool m);

By default, newly opened handles operate in blocking mode. A backend may
elect not to support non-blocking I/O mode. In such a case the status
value :macro:`SANE_STATUS_UNSUPPORTED` is returned. Blocking
I/O must be supported by all backends, so calling this function with
argument :data:`m` set to :macro:`SANE_FALSE` is
guaranteed to complete successfully.

This function may fail with one of the following status codes:

    :macro:`SANE_STATUS_INVAL`:
    No image acquisition is pending.

    :macro:`SANE_STATUS_UNSUPPORTED`:
    The backend does not support the requested I/O mode.

.. index:: sane_get_select_fd

:func:`sane_get_select_fd`
~~~~~~~~~~~~~~~~~~~~~~~~~~

This function is used to obtain a (platform-specific) file-descriptor
for handle :data:`h` that is readable if and only if image
data is available (i.e., when a call to :func:`sane_read()`
will return at least one byte of data). If the call completes
successfully, the select file-descriptor is returned in
:data:`*fd`.

    ::

        SANE_Status sane_get_select_fd (SANE_Handle h, SANE_Int *fd);

This function can be called only after a call to
:func:`sane_start()` has been performed and the returned
file-descriptor is guaranteed to remain valid for the duration of the
current image acquisition (i.e., until :func:`sane_cancel()`
or :func:`sane_start()` get called again or until
:func:`sane_read()` returns with status
:macro:`SANE_STATUS_EOF`). Indeed, a backend must guarantee
to close the returned select file descriptor at the point when the next
:func:`sane_read()` call would return
:macro:`SANE_STATUS_EOF`. This is necessary to ensure the
application can detect when this condition occurs without actually
having to call :func:`sane_read()`.

A backend may elect not to support this operation. In such a case, the
function returns with status code
:macro:`SANE_STATUS_UNSUPPORTED`.

Note that the only operation supported by the returned file-descriptor
is a host operating-system dependent test whether the file-descriptor is
readable (e.g., this test can be implemented using
:func:`select()` or :func:`poll()` under UNIX).
If any other operation is performed on the file descriptor, the behavior
of the backend becomes unpredictable. Once the file-descriptor signals
“readable” status, it will remain in that state until a call to
:func:`sane_read()` is performed. Since many input devices
are very slow, support for this operation is strongly encouraged as it
permits an application to do other work while image acquisition is in
progress.

This function may fail with one of the following status codes:

    :macro:`SANE_STATUS_INVAL`:
    No image acquisition is pending.

    :macro:`SANE_STATUS_UNSUPPORTED`:
    The backend does not support this operation.

.. index:: sane_strstatus

:func:`sane_strstatus`
~~~~~~~~~~~~~~~~~~~~~~

This function can be used to translate a SANE status code into a
printable string. The returned string is a single line of text that
forms a complete sentence, but without the trailing period (full-stop).
The function is guaranteed to never return :macro:`NULL`. The
returned pointer is valid at least until the next call to this function
is performed.

    ::

        SANE_String_Const sane_strstatus (SANE_Status status);

.. index:: code flow

Code Flow
---------

The code flow for the SANE API is illustrated in
:numref:`fig:flow`. Functions
:func:`sane_init()` and :func:`sane_exit()`
initialize and exit the backend, respectively. All other calls must be
performed after initialization and before exiting the backend.

.. figure:: figs/flow.*
   :name: fig:flow
   :scale: 50%
   :align: center

   Code flow

Function :func:`sane_get_devices()` can be called any time
after :func:`sane_init()` has been called. It returns the
list of the devices that are known at the time of the call. This list
may change over time since some devices may be turned on or off or a
remote host may boot or shutdown between different calls. It should be
noted that this operation may be relatively slow since it requires
contacting all configured devices (some of which may be on remote
hosts). A frontend may therefore want to provide the ability for a user
to directly select a desired device without requiring a call to this
function.

Once a device has been chosen, it is opened using a call to
:func:`sane_open()`. Multiple devices can be open at any
given time. A SANE backend must not impose artificial constraints on how
many devices can be open at any given time.

An opened device can be setup through the corresponding device handle
using functions :func:`sane_get_option_descriptor()` and
:func:`sane_control_option()`. While setting up a device,
obtaining option descriptors and setting and reading of option values
can be mixed freely. It is typical for a frontend to read out all
available options at the beginning and then build a dialog (either
graphical or a command-line oriented option list) that allows to control
the available options. It should be noted that the number of options is
fixed for a given handle. However, as options are set, other options may
become active or inactive. Thus, after setting an option, it maybe
necessary to re-read some or all option descriptors. While setting up
the device, it is also admissible to call
:func:`sane_get_parameters()` to get an estimate of what the
image parameters will look like once image acquisition begins.

The device handle can be put in blocking or non-blocking mode by a call
to :func:`sane_set_io_mode()`. Devices are required to
support blocking mode (which is the default mode), but support for
non-blocking I/O is strongly encouraged for operating systems such as
UNIX.

After the device is setup properly, image acquisition can be started by
a call to :func:`sane_start()`. The backend calculates the
exact image parameters at this point. So future calls to
:func:`sane_get_parameters()` will return the exact values,
rather than estimates. Whether the physical image acquisition starts at
this point or during the first call to :func:`sane_read()`
is unspecified by the SANE API. If non-blocking I/O and/or a
select-style interface is desired, the frontend may attempt to call
:func:`sane_set_io_mode()` and/or
:func:`sane_get_select_fd()` at this point. Either of these
functions may fail if the backend does not support the requested
operation.

Image data is collected by repeatedly calling
:func:`sane_read()`. Eventually, this function will return
an end-of-file status (:macro:`SANE_STATUS_EOF`). This
indicates the end of the current frame. If the frontend expects
additional frames (e.g., the individual channels of a red/green/blue
image or multiple images), it can call :func:`sane_start()`
again. Once all desired frames have been acquired, function
:func:`sane_cancel()` must be called. This operation can
also be called at any other time to cancel a pending operation. Note
that :func:`sane_cancel()` must be called even if the last
read operation returned :macro:`SANE_STATUS_EOF`.

When done using the device, the handle should be closed by a call to
:func:`sane_close()`. Finally, before exiting the
application, function :func:`sane_exit()` must be called. It
is important not to forget to call this function since otherwise some
resources (e.g., temporary files or locks) may remain unclaimed.

.. index:: well known options

Well-Known Options
------------------

While most backend options are completely self-describing, there are
cases where a user interface might want to special-case the handling of
certain options. For example, the scan area is typically defined by four
options that specify the top-left and bottom-right corners of the area.
With a graphical user interface, it would be tedious to force the user
to type in these four numbers. Instead, most such interfaces will want
to present to the user a preview (low-resolution scan) of the scanner
surface and let the user pick the scan area by dragging a rectangle into
the desired position. For this reason, the SANE API specifies a small
number of option names that have well-defined meanings.

.. index:: option count

Option Number Count
~~~~~~~~~~~~~~~~~~~

Option number 0 has an empty string as its name. The value of this
option is of type :macro:`SANE_TYPE_INT` and it specifies the
total number of options available for a given device (the count includes
option number 0). This means that there are two ways of counting the
number of options available: a frontend can either cycle through all
option numbers starting at one until
:func:`sane_get_option_descriptor()` returns
:macro:`NULL`, or a frontend can directly read out the value
of option number 0.

.. index:: scan resolution, resolution option

Scan Resolution Option
~~~~~~~~~~~~~~~~~~~~~~

Option ``resolution`` is used to select the resolution
at which an image should be acquired. The type of this option is either
:macro:`SANE_TYPE_INT` or
:macro:`SANE_TYPE_FIXED`. The unit is
:macro:`SANE_UNIT_DPI` (dots/inch).

This option is not mandatory, but if a backend does support it, it must
implement it in a manner consistent with the above definition.

.. index:: preview mode

Preview Mode Option
~~~~~~~~~~~~~~~~~~~

The Boolean option ``preview`` is used by a frontend to
inform the backend when image acquisition should be optimized for speed,
rather than quality (“preview mode”). When set to
:macro:`SANE_TRUE`, preview mode is in effect, when set to
:macro:`SANE_FALSE` image acquisition should proceed in
normal quality mode. The setting of this option *must not* affect any
other option. That is, as far as the other options are concerned, the
preview mode is completely side effect free. A backend can assume that
the frontend will take care of appropriately setting the scan resolution
for preview mode (through option ``resolution``). A
backend is free to override the ``resolution`` value
with its own choice for preview mode, but it is advised to leave this
choice to the frontend wherever possible.

This option is not mandatory, but if a backend does support it, it must
implement it in a manner consistent with the above definition.

.. index:: scan area options

Scan Area Options
~~~~~~~~~~~~~~~~~

The four most important well-known options are the ones that define the
scan area. The scan area is defined by two points (x/y coordinate pairs)
that specify the top-left and the bottom-right corners. This is
illustrated in :numref:`fig:area`. Note that the origin
of the coordinate system is at the top-left corner of the scan surface
as seen by the sensor (which typically is a mirror image of the scan
surface seen by the user). For this reason, the top-left corner is the
corner for which the abscissa and ordinate values are simultaneously the
*smallest* and the bottom-right corner is the corner for which the
abscissa and ordinate values are simultaneously the *largest*. If this
coordinate system is not natural for a given device, it is the job of
the backend to perform the necessary conversions.

.. figure:: figs/area.*
   :name: fig:area
   :scale: 90%
   :align: center

   Scan area options

The names of the four options that define the scan area are given in the
table below:

.. index:: tl-x, tl-y, br-x, br-y

.. tabularcolumns:: |L|L|
.. table::
   :name: scan-area
   :align: center

   +----------+-----------------------------------------+
   | Name     | Description                             |
   +==========+=========================================+
   | ``tl-x`` | Top-left :math:`x` coordinate value     |
   +----------+-----------------------------------------+
   | ``tl-y`` | Top-left :math:`y` coordinate value     |
   +----------+-----------------------------------------+
   | ``br-x`` | Bottom-right :math:`x` coordinate value |
   +----------+-----------------------------------------+
   | ``br-y`` | Bottom-right :math:`y` coordinate value |
   +----------+-----------------------------------------+

There are several rules that should be followed by front and backends
regarding these options:

-  Backends must attach a unit of either pixels
   (:macro:`SANE_UNIT_PIXEL`) or millimeters
   (:macro:`SANE_UNIT_MM`) to these options. The unit of all
   four options must be identical.

-  Whenever meaningful, a backend should attach a range or a word-list
   constraint to these options.

-  A frontend can determine the size of the scan surface by first
   checking that the options have range constraints associated. If a
   range or word-list constraints exist, the frontend can take the
   minimum and maximum values of one of the x and y option
   range-constraints to determine the scan surface size.

-  A frontend must work properly with any or all of these options
   missing.

.. [1]
   This is different from ANSI C where any non-zero integer value
   represents logical TRUE.
